import { Page } from "tns-core-modules/ui/page/page";


var reflect = require("reflect-metadata");
var component = require("../app/home/home.component");

describe("Tests for app/home/home.component.ts", function () {
  var homeComponent;
  beforeEach(function () {
    homeComponent = new component.HomeComponent(Page);
  });

  it("Verify Button Seach Defaul Message", function () {
    expect(homeComponent.findLocationsBtn.text).toBe("FIND SOMEWHERE TO PLAY");
  });

});