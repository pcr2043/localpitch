import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";



@Injectable({
    providedIn: "root"
})

export class DataService {



    constructor(private http: HttpClient) {

    }

    getVenues(location: string) {
        let headers = this.createRequestHeader();

        let endpoint: string = "https://api-v2.mylp.info/venues?filter[name]=" + location + "&filter[city]=london&filter[sport]=&page[size]=2&include=pitches";

        return this.http.get(endpoint, { headers: headers }).pipe(map((res: any) => {
            return res.included;
        }))

    }

    getPitch(pitchID: Number) {

        let headers = this.createRequestHeader();

        let endpoint = "https://api-v2.mylp.info/pitches/" + pitchID + "?include=venues";

        return this.http.get(endpoint, { headers: headers });

    }

    private createRequestHeader() {
        // set headers here e.g.
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
        });

        return headers;
    }

}
