import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { DataService } from '../dataService.service';
import { RouterExtensions } from 'nativescript-angular/router';


@Component({
  selector: 'ns-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  moduleId: module.id,
})

export class DetailsComponent implements OnInit {
  public pitchID: number;
  public image: string;
  public pitch: any;
  public interval:any;

  public constructor(private route: ActivatedRoute, private router: RouterExtensions, public dataService: DataService) {
   
    // get pitch param
    this.route.params.subscribe((params) => {
      this.pitchID = params["pitch"];
    });

    // get the pitch from the dataService
    this.dataService.getPitch(this.pitchID).subscribe(pitch => {
      this.pitch = pitch;
      this.bindImages();
    })

  }
  ngOnInit() {


  }

  bindImages() {

    // some control to switch image can be improved to add same fade effect
    // or a slider will be better idea
    if (this.pitch.data.attributes.images.large.length > 0) {

      this.image = this.pitch.data.attributes.images.large[0];
      let index = 0;
      let local = this;

     this.interval =  setInterval(function () {
      
      if (index < (local.pitch.data.attributes.images.large.length - 1))
          index++;
        else
          index = 0;

        local.image = local.pitch.data.attributes.images.large[index];

      }, 5000)
    }
  }

  tapGoBack() {
    clearInterval(this.interval)
    this.router.back();
  }

}
