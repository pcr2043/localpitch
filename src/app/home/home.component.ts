import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { Page } from 'tns-core-modules/ui/page/page';

//import the service that contains the methods dot he localPitchApi
import { DataService } from '../dataService.service';

@Component({
  selector: 'ns-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  moduleId: module.id,
})


export class HomeComponent {

  // declare a member or variable string for the location  for the search
  public location: string = ''

  // declare a member string for the button text as we can use to display some messages like loading etc...
  public findLocationsBtn: any = {
    text: 'FIND SOMEWHERE TO PLAY'
  }

  // an array member for venus results
  public venues: Array<any> = [];

  constructor(page: Page, private router: Router, public dataService: DataService) {

    // hide the actionBar
    page.actionBarHidden = true;
  }

  findLocations() {

    // set button test message
    this.findLocationsBtn.text = "PLEASE WAIT ...";

    // call the service method getVenues by using the location entered in the searchbox Input
    this.dataService.getVenues(this.location).subscribe((data: any) => {

      // if nothing is returned or results are array is empty, display no results msg
      // else bind the results and set button message total entries found
      if (data == undefined || (data != undefined && data.isArray && data.length == 0))
        this.findLocationsBtn.text = "NO RESULTS FOUND ...";
      else {
        this.venues = data;
        this.findLocationsBtn.text = this.venues.length + " RESULTS FOUND ...";
      }

    }, (error) => {

      // in case of error set some text to the button
      this.findLocationsBtn.text = "NO RESULTS FOUND ...";
    })

  }

  selectPitch(pitch) {
    // navigate to the dettails view route /pitch/{pitchID}
    this.router.navigate(["pitch", pitch.id]);
  }

}
